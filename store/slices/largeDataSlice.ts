import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import type { AppState, AppThunk } from '../index'

const fetchData: ()=>Promise<{data:Record<string, any>[]}> = async () => {
    let loadUrl = `${process.env.NEXT_PUBLIC_SERVER}/api/large-data`
    const res = await fetch(loadUrl)
    return res.json()
}

export interface LargeDataState {
  isLoading: boolean
  loaded: boolean
  data: Record<string, any>[]
}

const initialState: LargeDataState = {
  isLoading:false,
  loaded: false,
  data:[],
}

export const fetchDataAsync = createAsyncThunk(
  'counter/fetchData',
  async () => {
    let output = [] as Record<string,any>[]

    const result = await fetchData()
    for (let i = 0; i < 10; i++) {
        if(result.data && result.data.length > 0) {
            output = [...output, ...result.data]
        }
    }
    return output
  }
)

export const largeDataSlice = createSlice({
  name: 'largeData',
  initialState,
  reducers: {
    resetData: (state)=> {
        state.isLoading=false,
        state.loaded= false,
        state.data=[]
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchDataAsync.pending, (state, action) => {
        state.isLoading = true
      })
      .addCase(fetchDataAsync.fulfilled, (state, action) => {
        state.isLoading = false
        state.loaded = true
        state.data = action.payload
      })
      .addCase(fetchDataAsync.rejected, (state, action) => {
        state.isLoading = false
        state.loaded = true
        state.data = []
      })
  },
})

export const isLoading = (state: AppState) => state.largeData.isLoading
export const isLoaded = (state: AppState) => state.largeData.loaded
export const getLargeData = (state: AppState) => state.largeData.data
export const {resetData} = largeDataSlice.actions 

export default largeDataSlice.reducer
