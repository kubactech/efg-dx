import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'

import largeDataSlice from './slices/largeDataSlice'

export function makeStore() {
  return configureStore({
    reducer: {largeData: largeDataSlice},
  })
}

const store = makeStore()

export type AppState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>

export default store
