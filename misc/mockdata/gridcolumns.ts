//@ts-nocheck
export const generateColumnData = (user) => ([
    {colAccess:["full","limited"],dataField:"EmployeeID"},
    {colAccess:["full","limited"],dataField:"FullName", allowEditing:false},
    {colAccess:["full","limited"], dataField:"Position", groupIndex: user.role === 'user' && 0 },
    {colAccess:["full","limited"],dataField:"TitleOfCourtesy"},
    {colAccess:["full"],dataField:"BirthDate", dataType:"date"},
    {colAccess:["full"],dataField:"HireDate", dataType:"date"},
    {colAccess:["full","limited"],dataField:"Address"},
    {colAccess:["full","limited"],dataField:"City"},
    // {colAccess:["full","limited"],dataField:"Region"},
    // {colAccess:["full","limited"],dataField:"PostalCode"},
    {colAccess:["full","limited"],dataField:"Country"},
    // {colAccess:["full"],dataField:"HomePhone"},
    // {colAccess:["full"],dataField:"Extension"},
    // {colAccess:["full"],dataField:"Photo"},
    // {colAccess:["full"],dataField:"Notes"},
    // {colAccess:["full"],dataField:"ReportsTo"}
])

export const generateGridSettings = (user) => ({
    id:"firstGrid",
    keyExpr:["EmployeeID"],
    columnChooser: {
        enabled: user.gridFeatures.columnChooser,
        mode:"select",
        allowSearch:true,
    },
    columnFixing: {
        enabled:user.gridFeatures.columnFixing,
        texts:{
            fix:
            "Fix"
            ,
            leftPosition:
            "To the left"
            ,
            rightPosition:
            "To the right"
            ,
            unfix:
            "Unfix"
            },
    },
    allowColumnReordering:true,
    editing: {
        allowAdding:user.gridFeatures.editing.adding,
        allowDeleting: user.gridFeatures.editing.deleting,
        allowUpdating: user.gridFeatures.editing.editing,
        mode:"batch",
        newRowPosition:"viewportTop",
        useIcons:true,
    },
    // stateStoring: {
    //     enabled:user.gridFeatures.stateStoring,
    //     type:'localStorage',
    //     storageKey:`${user.user_id}.view`
    // },
    grouping:{
        contextMenuEnabled:user.gridFeatures.grouping
    }
})