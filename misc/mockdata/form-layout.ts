export const formData = {
    ID: 1,
    FirstName: 'John',
    LastName: 'Heart',
    CompanyName: 'Super Mart of the West',
    Position: 'CEO',
    OfficeNo: '901',
    BirthDate: new Date(1964, 2, 16),
    HireDate: new Date(1995, 0, 15),
    Address: '351 S Hill St.',
    City: 'Los Angeles',
    State: 'CA',
    Zipcode: '90013',
    Phone: '+1(213) 555-9392',
    Email: 'jheart@dx-email.com',
    Skype: 'jheart_DX_skype',
  };

  export const KarelItems = [
    {
        itemType:"group",
        caption:"Main Info",
        colSpan:2,
        items:[
            "FirstName",
            "LastName",
            {
                dataField:"CompanyName",
                disabled:true
            },
            "Position",
            "OfficeNo"
        ] 
    },
    {
        itemType:"tabbed",
        title:"Personal Info",
        tabs:[
            {
                title:"Bio",
                items: [
                    "BirthDate",
                    "HireDate",
                ]
            },
            {
                title:"Address",
                items: [
                    "Address",
                    "City",
                    "State",
                    "Zipcode"
                ]
            }
        ] 
    },
    {
        itemType:"tabbed",
        title:"Contact",
        tabs: [
            {
                title:"Main Contact",
                items: [
                    "Phone",
                ]
            },
            {
                title:"Secondary Contact",
                items: [
                    "Skype",
                    "Email"
                ]
            }
        ]
    }
  ]

  export const FrantaItems = [
    {
        itemType:"group",
        caption:"Main Info",
        items:[
            "FirstName",
            "LastName",
            "CompanyName",
            "Position",
            "OfficeNo"
        ] 
    },
    {
        itemType:"group",
        caption:"Personal Info",
        items:[
            "BirthDate",
            "HireDate",
            {
                itemType:"group",
                caption:"Address",
                items: [
                    "Address",
                    "City",
                    "State",
                    "Zipcode"
                ]
            }
        ] 
    },
    {
        itemType:"tabbed",
        title:"Contact",
        tabs: [
            {
                title:"Main Contact",
                items: [
                    "Phone",
                ]
            },
            {
                title:"Secondary Contact",
                items: [
                    "Skype",
                    "Email"
                ]
            }
        ]
    }
  ]