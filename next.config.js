/** @type {import('next').NextConfig} */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const securityHeaders = [
  {key:"Access-Control-Allow-Origin",value:"*"}
]
const nextConfig = withBundleAnalyzer({
  reactStrictMode: false,
  swcMinify: true,
  async headers() {
    return [
      {
        source: '/:path*',
        headers: securityHeaders,
      }
    ]
  }
})

module.exports = nextConfig
