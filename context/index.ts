import React, {useCallback, useMemo, useReducer} from "react"
import GridReducer from "./reducers/grid"
import ThemeReducer from './reducers/theme'

export type Action<P=unknown> = {type:string, payload?: P}

const combineDispatch = (...dispatches: Array<(action:Action)=>void>) => (action:Action) =>
  dispatches.forEach((dispatch) => dispatch(action));

const useAppContext = () =>{
    const [grid, d1] = GridReducer();
    const [theme, d2] = ThemeReducer()
    const combinedDispatch = useCallback(()=>combineDispatch(d1, d2), [d1, d2]);
    const combinedState = useMemo(() => ({ grid, theme, }), [grid, theme]);

    return [combinedDispatch(),combinedState] as [(action:Action)=>void, typeof combinedState]
}


export const StateContext = React.createContext({} as Record<string,any>)
export const DispatchContext = React.createContext((action:Action)=>{})

export default useAppContext