/* eslint-disable import/no-anonymous-default-export */

import React from "react"
const initialState = {
    dark:true
}

const reducer = (state = initialState, action:{type:string}) =>{
    switch (action.type) {
        case 'SET_LIGHT':
            return {...state, dark:false}            
    
        default:
            return state
    }
}

export default ()=> React.useReducer(reducer,initialState)