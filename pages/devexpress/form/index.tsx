import React, {useRef, useState} from 'react'
import Form, { Item, SimpleItem, RequiredRule, CompareRule } from 'devextreme-react/form';
import dxForm from 'devextreme/ui/form';
import Layout from '../../../components/Layout';
import {Toast} from "devextreme-react/toast";


const formData = {
    name:'John Doe',
    position:undefined,
    startDate: new Date(),
    company:'Apple',
}

const positions = {
    items: [
    'tester',
    'developer',
    'programmer'
],
searchEnabled:true,
}

const DevExpressHome = () => {
    const formRef=useRef<undefined|dxForm>()
    const [submitted, setSubmitted] = useState(false)

  return (
      <Layout>
        <h3>Form and validation</h3>
        <Form
        formData={formData}
        labelMode="outside"
        labelLocation='top'
        id="form"
        onContentReady={(e)=>formRef.current = e.component}
        showValidationSummary
        >
            <Item dataField="name"/>
            <Item dataField="position" editorType="dxSelectBox" editorOptions={positions} >
                <RequiredRule message={"Please, select a position"}/>
            </Item>
            <Item dataField="startDate"/>
            <Item dataField="company">
                <CompareRule message={"Use correct company (EFG)"} comparisonTarget={()=>"EFG"}/>
            </Item>
            <Item itemType="button" horizontalAlignment="left" buttonOptions={{
                icon:"save",
                text:"submit me",
                type:"success",
                onClick:()=>{
                    const {isValid} =  (formRef.current as dxForm).validate()
                    if(isValid) {
                        console.log(formData)
                        setSubmitted(true)
                    }
                }
            }} />
        </Form>  
        <Toast
            type="success"
            message="Data submitted"
            visible={submitted}
            onHiding={()=> setSubmitted(false)}
            position="bottom center"
        />
        </Layout>
  )
}

export default DevExpressHome