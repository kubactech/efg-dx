import React, {useContext} from 'react'
import { Button} from 'devextreme-react/button';
import {
    DataGrid, Column, ColumnFixing
} from 'devextreme-react/data-grid';
import _ from "lodash"

import CustomStore from 'devextreme/data/custom_store';
import { StateContext } from '../../../context';
import Layout from '../../../components/Layout';


const customDataSource = new CustomStore({
    key:"EmployeeID",
    load:(loadOptions: any)=>{
        const {sort} = loadOptions
        let loadUrl = `${process.env.NEXT_PUBLIC_SERVER}/api/employees`
        if(sort && _.isArray(sort) && sort.length) {
           const col = sort[0].selector
           const order = sort[0].desc ? 'desc' : 'asc'
           loadUrl = `${loadUrl}?col=${col}&order=${order}`
        }
        return fetch(loadUrl)
        .then(response=> response.json())
    }
})

const Datagrid = () => {
    const {grid} = useContext(StateContext)
  return (
    <Layout>
        <h2>Datagrid</h2>
    <DataGrid 
    id="firstGrid"
    dataSource={customDataSource}
    keyExpr="EmployeeID"
    allowColumnReordering= {grid.allowColumnReordering}
    columns={["EmployeeID",{dataField:"FullName", width:200, fixed:grid.fixedNameCol},"Position", {dataField:"BirthDate", dataType:"date"}, "HireDate"]}
    columnAutoWidth
    columnFixing={{
        enabled:grid.columnFixing,
        texts:{
            fix:
            "Fix"
            ,
            leftPosition:
            "To the left"
            ,
            rightPosition:
            "To the right"
            ,
            unfix:
            "Unfix"
            },
    }}
    columnChooser={{
        enabled:grid.columnChooser,
        mode:"select"
    }}
    searchPanel={{
        visible:grid.searchPanel
    }}
    editing={{
        allowAdding:grid.allowAdding,
        mode:'row'
    }}
    remoteOperations={{
        sorting:true,
    }}
    >
        
    </DataGrid>
    </Layout>
  )
}

export default Datagrid