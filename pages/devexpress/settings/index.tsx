import React, {useContext, useRef, useEffect, useState} from 'react'
import {
    Form,
    SimpleItem,
    EmptyItem
} from 'devextreme-react/form';
import { DispatchContext, StateContext } from '../../../context';
import { setGridSettings } from '../../../context/reducers/grid';
import DxSwitch from 'devextreme/ui/switch';
import { Button } from 'devextreme-react/button';
import Link from 'next/link';
import Layout from '../../../components/Layout';


const Settings = () => {
 const state = useContext(StateContext)
 const dispatch = useContext(DispatchContext)
 const formRef = useRef({...state.grid})
 formRef.current = {...state.grid}

  return (
   <Layout>
        <h2>Settings</h2>
        <h3> Data grid</h3>
        <Form
        formData={formRef.current}
        >
            <SimpleItem dataField={"fixedNameCol"} editorType={DxSwitch} label={{text:"Fixed name column"}}/>
            <SimpleItem dataField={"allowColumnReordering"} editorType={DxSwitch} label={{text:"Allow column reordering"}}/>
            <SimpleItem dataField={"columnAutoWidth"} editorType={DxSwitch} />
            <SimpleItem dataField={"columnFixing"} editorType={DxSwitch} label={{text:"Allow fixing collumns"}}/>
            <SimpleItem dataField={"columnChooser"} editorType={DxSwitch} label={{text:"Enable column chooser feature"}}/>
            <SimpleItem dataField={"searchPanel"} editorType={DxSwitch} />
            <SimpleItem dataField={"allowAdding"} editorType={DxSwitch} label={{text:"Enable adding data"}}/>
        </Form>
        <div><Button icon="save" type="success" stylingMode='contained' onClick={()=>setGridSettings(dispatch,{...formRef.current})} text="Save"/></div>
    </Layout>
  )
}

export default Settings