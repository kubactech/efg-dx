//@ts-nocheck

import React, {useState} from 'react';
import 'devextreme-react/text-area';

import Form, {
  SimpleItem, GroupItem, TabbedItem, TabPanelOptions, Tab,
} from 'devextreme-react/form';
import DropDownButton from 'devextreme-react/drop-down-button';
import {formData, KarelItems, FrantaItems} from "../../../misc/mockdata/form-layout"
import Layout from '../../../components/Layout';

const ITEMS = {
  franta: FrantaItems,
  karel: KarelItems
}


const FormLayout = ()=> {
  const [selectedUser,setSelectedUser]=useState<'karel'|'franta'>('karel')
  const userItems = ITEMS[selectedUser]

    return (
        <Layout>
        <div style={{margin:"0 0 0 auto", width:200}}> 
         <DropDownButton
        text="Select user"
        icon="user"
        items={["franta", "karel"]} 
        style={{marginBottom:20}}
        onItemClick={({itemData})=> setSelectedUser(itemData)}
        selectedItem="franta"
        />
        <h5>Selected user: {selectedUser} </h5>
        </div> 
        <div className="long-title"><h3>Personal details</h3></div>
        <div className="form-container">
          <Form
            colCount={2}
            id="form"
            formData={formData}
            items={userItems}
            />  
        </div>
      </Layout>
    );
}

export default FormLayout;
