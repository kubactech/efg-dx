import React, {useContext, useEffect} from 'react'
import { Button} from 'devextreme-react/button';
import {
    DataGrid, Column, ColumnFixing
} from 'devextreme-react/data-grid';
import _ from "lodash"

import CustomStore from 'devextreme/data/custom_store';
import Layout from '../../../components/Layout';
import { fetchDataAsync, getLargeData, isLoaded, isLoading, resetData } from '../../../store/slices/largeDataSlice';
import { useAppDispatch, useAppSelector } from '../../../store/hooks';
import { LoadIndicator } from 'devextreme-react';
import styles from '../../../styles/devexpress.module.css'

const customDataSource = new CustomStore({
    key:"id",
    load:(loadOptions: any)=>{
        const {sort} = loadOptions
        let loadUrl = `${process.env.NEXT_PUBLIC_SERVER}/api/large-data`
        // if(sort && _.isArray(sort) && sort.length) {
        //    const col = sort[0].selector
        //    const order = sort[0].desc ? 'desc' : 'asc'
        //    loadUrl = `${loadUrl}?col=${col}&order=${order}`
        // }
        return fetch(loadUrl)
        .then(response=> response.json())
    }
})

export const columns = [
    "id",
    "email",
    "username",
    {dataField:"profile.name"},
    {dataField:"profile.dob", dataType:"date", caption:"Date of birth"},
    "roles",
    {dataField:"createdAt", dataType:"date"},
    {dataField:"updatedAt", dataType:"date"},
]

const Datagrid = () => {
    const dispatch = useAppDispatch()
    const data = useAppSelector(getLargeData)
    const areDataLoaded = useAppSelector(isLoaded)
    const isLoadingData = useAppSelector(isLoading)

    console.log("data",data)

  return (
    <Layout>
        <h2>Datagrid</h2>
        <div className={styles.loadingButtonContainer}>
            <Button
            type='success'
            icon="upload"
            width={180}
            height={60}
            onClick={()=> dispatch(fetchDataAsync())}
            className={styles.loadingButton}
            disabled={areDataLoaded}
            >
                <LoadIndicator className={styles.loader} visible={isLoadingData} />
                <span className="dx-button-text">Load data</span>
            </Button>
            <Button
            type='danger'
            icon="refresh"
            width={180}
            height={60}
            onClick={()=> dispatch(resetData())}
            className={styles.loadingButton}
            disabled={!areDataLoaded}
            >
                <span className="dx-button-text">Reset</span>
            </Button>
        </div>
        <div>Loaded <span style={{fontWeight:'bold'}}>{data.length}</span> items</div>
    <DataGrid 
    id="id"
    dataSource={data}
    keyExpr="email"
    allowColumnReordering
    columnAutoWidth
    columnChooser={{
        enabled:true,
        mode:"select"
    }}
    searchPanel={{
        visible:true
    }}
    editing={{
        allowAdding:true,
        mode:'row'
    }}
    //@ts-ignore
    columns={columns}
    // remoteOperations={{
    //     sorting:true,
    // }}
    >
        
    </DataGrid>
    </Layout>
  )
}

export default Datagrid