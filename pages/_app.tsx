import React, {useEffect} from 'react'
import { Provider } from 'react-redux'

import '../styles/globals.css'
import '../styles/kanban.css'
import type { AppProps } from 'next/app'
import useAppContext, {StateContext, DispatchContext} from '../context';
import store from "../store"

export default function App({ Component, pageProps }: AppProps) {
  const [combinedDispatch, combinedState] = useAppContext()
  return (
    <Provider store={store}> 
      <StateContext.Provider value={combinedState}>
        <DispatchContext.Provider value={combinedDispatch}>
          <Component {...pageProps} />
        </DispatchContext.Provider>
      </StateContext.Provider>
    </Provider>
  )
  
}
