import React from 'react'
import { Html, Head, Main, NextScript } from 'next/document'

const Document = () => {
  return (
    <Html>
    <Head>
    <link rel="dx-theme" data-theme="generic.light" href="/dx.light.css" data-active="true"/>
    <link rel="dx-theme" data-theme="generic.dark" href="/dx.dark.css" data-active="false"/>
    </Head>
    <body className="dx-viewport">
      <Main />
      <NextScript />
    </body>
  </Html>
)
}

export default Document