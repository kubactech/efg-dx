// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import {employees} from "../../misc/mockdata/data"
import _ from "lodash"

const delayer =(ms:number)=> new Promise(resolve=> setTimeout(resolve,ms)) 

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof employees>
) {
    let result = employees
    const {col, order} = req.query
    const ordering = order === 'desc' ? 'desc' : 'asc' 
    if(col){
        result = _.orderBy(employees,col,ordering)
    }
    await delayer(1500)
  res.status(200).json(result)
}
