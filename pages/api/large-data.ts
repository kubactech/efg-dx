// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

const get = axios.create({
    baseURL: 'https://api.json-generator.com/templates/',
    headers: {"Authorization":"Bearer bkh4tfjouimrddz1m66vli0jtk88ltyp50d5u36w"}
  });

  export const config = {
    api: {
      responseLimit: false,
    },
  }
  

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<{data:Record<string,any>[]}>
) {
    try {
        let response = [] as Record<string,any>[]
    const dataSlots = await Promise.all([
        get('X0BjTzy65oQ9/data'),
        get('hRbQnlfBN9F5/data'),
        get('lmpfIJPoKtm3/data'),
        get('SLMN3_fyhytU/data'),
        get('ZtBfFbkavyjV/data'),
        get('1KNUnMq5ABOL/data'),
        get('_WvGlyel3rvZ/data'),
        get('RO07zRNiDCyn/data'),
    ])
    
    dataSlots.forEach(slot=> response.push(...slot.data)) 
    
  res.status(200).json({ data:response})

    } catch (error) {
    //@ts-ignore
    res.status(500).json({ error: error})
    }

}
