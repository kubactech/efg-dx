// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import _ from "lodash"
import { FrantaConfig, KarelConfig } from '../../../misc/mockdata/data'

const CONFIG = {
    karel:KarelConfig,
    franta: FrantaConfig
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
    const {u} = req.query 
    
    //@ts-ignore
  res.status(200).json({...CONFIG[u]})
}
