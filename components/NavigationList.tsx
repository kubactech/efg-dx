import React from 'react'
import { useRouter } from 'next/router'
import List from 'devextreme-react/list.js';

export const navigation = [
    { id: 1, text: 'Form', icon: 'fields', link:'/devexpress/form' },
    { id: 2, text: 'Form with layout', icon: 'fields', link:'/devexpress/form-layout' },
    { id: 3, text: 'Data Grid', icon: 'splitcells', link:'/devexpress/datagrid' },
    // { id: 4, text: 'Data Grid with users', icon: 'splitcells', link:'/devexpress/datagrid-users' },
    { id: 5, text: 'LARGE Datagrid', icon: 'splitcells', link:'/devexpress/datagrid-large' },
    { id: 6, text: 'Settings', icon: 'preferences', link:'/devexpress/settings' },
  ];

const NavigationList = () => {
    const router = useRouter()
    const selectedItems = navigation.filter((nav)=>nav.link === router.pathname)
  return (
    <div className='list' style={{ width: '200px' }}>
        <List
          dataSource={navigation}
          hoverStateEnabled={true}
          activeStateEnabled={true}
          focusStateEnabled={false}
          className="panel-list dx-theme-accent-as-background-color" 
          // @ts-ignore
          onItemClick={(e)=> router.push(e.itemData?.link)}
          selectionMode="single"
          selectedItems={selectedItems}/>
    </div>
  )
}

export default NavigationList