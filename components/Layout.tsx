import React, {useState, FC, ReactNode} from 'react'
import themes  from "devextreme/ui/themes";
import { Button } from 'devextreme-react/button';

import Toolbar from 'devextreme-react/toolbar';
import Drawer from 'devextreme-react/drawer';

import styles from '../styles/devexpress.module.css'
import NavigationList from './NavigationList'

interface Props {
    children:ReactNode
}

enum THEMES {
    LIGHT = 'generic.light',
    DARK = 'generic.dark'
}

const Layout: FC<Props> = ({children}) => {
    const [opened, setOpened] = useState(false)
    const [theme,setTheme] = useState(THEMES.DARK)

    const toolbarItems = [{
        widget: 'dxButton',
        location: 'before',
        options: {
          icon: 'menu',
          onClick: () => setOpened((prevState)=> !prevState),
        },
        },
        {
            widget: 'dxButton',
            location: 'before',
            options: {
              icon: 'palette',
              onClick: () => {
                themes.current(theme)
                if(theme === THEMES.DARK) {
                    setTheme(THEMES.LIGHT)
                } else {
                    setTheme(THEMES.DARK)
                }
              },
            },
        }];
  return (
    <div className={styles.layoutContainer}>
     <Toolbar  items={toolbarItems } />
    <Drawer
          opened={opened}
          openedStateMode="shrink"
          position="left"
          revealMode="slide"
          component={NavigationList}>
    <div className={styles.container}>
        {children}
    </div>
    </Drawer>
    </div>
  )
}

export default Layout